package com.genesys.challenge.mapper;

import com.genesys.challenge.models.Course;
import com.genesys.challenge.request.CreateCourseRequest;

import java.util.UUID;

public class CourseMapper {


    public static Course courseRequestToDao(CreateCourseRequest courseRequest) {
        return Course.builder()
                .id(UUID.randomUUID())
                .courseName(courseRequest.getCourseName())
                .startDate(courseRequest.getStartDate())
                .endDate(courseRequest.getEndDate())
                .maximumCapacity(courseRequest.getMaximumCapacity())
                .build();
    }

}
