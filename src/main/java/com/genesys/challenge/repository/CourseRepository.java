package com.genesys.challenge.repository;

import com.genesys.challenge.models.Course;
import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface CourseRepository extends CassandraRepository<Course, UUID> {
}
