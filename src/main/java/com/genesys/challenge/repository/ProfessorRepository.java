package com.genesys.challenge.repository;

import com.genesys.challenge.models.Professor;
import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface ProfessorRepository extends CassandraRepository<Professor, UUID> {
}
