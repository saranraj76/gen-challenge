package com.genesys.challenge.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CreateCourseRequest {

    private String courseName;
    private Date startDate;
    private Date endDate;
    private int maximumCapacity;

}
