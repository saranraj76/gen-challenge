package com.genesys.challenge.service;

import com.genesys.challenge.models.Professor;
import com.genesys.challenge.repository.ProfessorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class ProfessorService {

    @Autowired
    ProfessorRepository professorRepository;

    public Professor createProfessor(Professor professor){
        professor.setId(UUID.randomUUID());
        return professorRepository.save(professor);
    }

}
