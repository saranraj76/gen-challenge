package com.genesys.challenge.service;

import com.genesys.challenge.exception.CapacityExceededException;
import com.genesys.challenge.mapper.CourseMapper;
import com.genesys.challenge.models.Course;
import com.genesys.challenge.models.ProfessorUdt;
import com.genesys.challenge.models.Student;
import com.genesys.challenge.models.StudentUdt;
import com.genesys.challenge.repository.CourseRepository;
import com.genesys.challenge.repository.StudentRepository;
import com.genesys.challenge.request.CreateCourseRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

@Component
public class CourseService {

    @Autowired
    CourseRepository courseRepository;

    public Course addcourse(CreateCourseRequest courseRequest){
        return courseRepository.save(CourseMapper.courseRequestToDao(courseRequest));
    }

    public Optional<Course> getCourse(String id){
        return courseRepository.findById(UUID.fromString(id));
    }

    public void addProfessor(String courseId, ProfessorUdt professor){
        Optional<Course> course = courseRepository.findById(UUID.fromString(courseId));
        if (course.isPresent()) {
            Set<ProfessorUdt> professors;
            if (null == course.get().getProfessors()) {
                professors = new HashSet<>();
            } else {
                professors = course.get().getProfessors();
            }
            professors.add(professor);
            course.get().setProfessors(professors);
            courseRepository.save(course.get());
        }
    }

    /*

    eventual consistency of the cassandra could have some race issues
    the other approach is to write the course student relation in a seperate table like student_by_course
    anyway duplication & writes are cassandra's strength
     */

    public void addStudent(String courseId, StudentUdt student) {
        Optional<Course> course = courseRepository.findById(UUID.fromString(courseId));
        if (course.isPresent()) {
            Set<StudentUdt> students;
            if (null == course.get().getEnrolledStudents()) {
                students = new HashSet<>();
            } else {
                students = course.get().getEnrolledStudents();
            }
            if (course.get().getMaximumCapacity() > students.size()) {
                students.add(student);
                course.get().setEnrolledStudents(students);
                courseRepository.save(course.get());
            }
            else {
                throw new CapacityExceededException();
            }
        }
    }

}
