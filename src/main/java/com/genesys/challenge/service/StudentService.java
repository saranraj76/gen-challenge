package com.genesys.challenge.service;

import com.genesys.challenge.models.Student;
import com.genesys.challenge.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class StudentService {

    @Autowired
    StudentRepository studentRepository;

    public Student createStudent(Student student){
        student.setId(UUID.randomUUID());
        return studentRepository.save(student);
    }

}
