package com.genesys.challenge.exception;

public class CapacityExceededException extends RuntimeException{
    public static String ERROR_MESSAGE = "COURSE CAPACITY EXCEEDED";
    public static String INTERNAL_CODE = "COURSE_CAPACITY_EXCEEDED";

    public CapacityExceededException(){
        super(ERROR_MESSAGE);
    }
}
