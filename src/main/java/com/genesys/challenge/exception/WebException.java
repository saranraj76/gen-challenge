package com.genesys.challenge.exception;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
public class WebException {
    private final String userMessage;
    @Builder.Default
    private final InternalErrorCode internalErrorCode = InternalErrorCode.INTERNAL_SERVER_ERROR;

}
