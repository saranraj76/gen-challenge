package com.genesys.challenge.exception;

public class DataBaseException  extends RuntimeException{

    public static String ERROR_MESSAGE = "Error while saving to Database";
    public static String INTERNAL_CODE = "DATABASE_ERROR";

    public DataBaseException(String message, Exception exception){
        super(message, exception);
    }
}
