package com.genesys.challenge.exception;

public enum InternalErrorCode {
    INTERNAL_SERVER_ERROR,
    CAPACITY_EXCEEDED
}
