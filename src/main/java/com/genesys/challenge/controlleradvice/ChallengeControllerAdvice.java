package com.genesys.challenge.controlleradvice;


import com.genesys.challenge.controller.CourseController;
import com.genesys.challenge.controller.ProfessorController;
import com.genesys.challenge.controller.StudentController;
import com.genesys.challenge.exception.CapacityExceededException;
import com.genesys.challenge.exception.InternalErrorCode;
import com.genesys.challenge.exception.WebException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice(assignableTypes = {CourseController.class, StudentController.class, ProfessorController.class})
public class ChallengeControllerAdvice {

    @ExceptionHandler(CapacityExceededException.class)
    @ResponseStatus(HttpStatus.FAILED_DEPENDENCY)
    @ResponseBody
    public WebException handleCapacityExceeded(CapacityExceededException exception){
        return WebException.builder()
                .internalErrorCode(InternalErrorCode.CAPACITY_EXCEEDED)
                .build();
    }


    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ResponseBody
    public WebException handleGenericErrors(Exception exception){
        return WebException.builder()
                .internalErrorCode(InternalErrorCode.INTERNAL_SERVER_ERROR)
                .build();
    }
}
