package com.genesys.challenge.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.cassandra.core.mapping.Table;

import java.util.Date;
import java.util.Set;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table
public class Course {

    @Id
    private UUID id;
    private String courseName;
    private Date startDate;
    private Date endDate;
    private int maximumCapacity;
    private Set<ProfessorUdt> professors;
    private Set<StudentUdt> enrolledStudents;

}
