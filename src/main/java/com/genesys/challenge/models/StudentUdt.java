package com.genesys.challenge.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.cassandra.core.mapping.UserDefinedType;

import java.util.UUID;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@UserDefinedType("student")
public class StudentUdt {
    private UUID id;
    private String name;
}
