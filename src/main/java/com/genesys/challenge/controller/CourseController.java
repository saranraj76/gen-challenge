package com.genesys.challenge.controller;

import com.genesys.challenge.models.Course;
import com.genesys.challenge.models.ProfessorUdt;
import com.genesys.challenge.models.StudentUdt;
import com.genesys.challenge.request.CreateCourseRequest;
import com.genesys.challenge.service.CourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
@RequestMapping("/api/v1/courses")
public class CourseController {

    @Autowired
    CourseService courseService;

    @PostMapping
    public Course createCourse(@RequestBody CreateCourseRequest createCourseRequest){
        return courseService.addcourse(createCourseRequest);
    }

    @GetMapping("/{id}")
    @ResponseBody
    public Optional<Course> getCourse(@PathVariable("id") String id){
        return courseService.getCourse(id);
    }

    @PostMapping("/{courseId}/professors")
    @ResponseStatus(HttpStatus.CREATED)
    public void addProfessor(@PathVariable String courseId, @RequestBody ProfessorUdt professorDetails){
        courseService.addProfessor(courseId,professorDetails);
    }

    @PostMapping("/{courseId}/students")
    @ResponseStatus(HttpStatus.CREATED)
    public void addStudent(@PathVariable String courseId, @RequestBody StudentUdt studentDetails){
        courseService.addStudent(courseId,studentDetails);
    }
}
