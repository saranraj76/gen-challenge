package com.genesys.challenge.controller;

import com.genesys.challenge.models.Student;
import com.genesys.challenge.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/students")
public class StudentController {

    @Autowired
    StudentService studentService;

    @PostMapping
    public Student createProfessor(@RequestBody Student student){

        return studentService.createStudent(student);

    }

}
