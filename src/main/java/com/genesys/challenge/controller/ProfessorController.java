package com.genesys.challenge.controller;

import com.genesys.challenge.models.Professor;
import com.genesys.challenge.service.ProfessorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/professors")
public class ProfessorController {

    @Autowired
    ProfessorService professorService;

    @PostMapping
    public Professor createProfessor(@RequestBody Professor professor){

        return professorService.createProfessor(professor);

    }

}
