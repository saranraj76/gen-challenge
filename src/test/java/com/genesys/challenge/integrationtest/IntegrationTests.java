package com.genesys.challenge.integrationtest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.genesys.challenge.models.Course;
import com.genesys.challenge.models.Professor;
import com.genesys.challenge.models.ProfessorUdt;
import com.genesys.challenge.models.Student;
import com.genesys.challenge.models.StudentUdt;
import com.genesys.challenge.request.CreateCourseRequest;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.Date;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
public class IntegrationTests {

        @Autowired
        MockMvc mockMvc;

        ObjectMapper mapper = new ObjectMapper();

        @SneakyThrows
        @Test
        public void testIntegrationTest(){
                CreateCourseRequest request = getCourseRequest();
              MvcResult resultCourse = mockMvc.perform(post("/api/v1/courses")
              .content(mapper.writeValueAsString(request))
                              .accept(MediaType.APPLICATION_JSON)
                      .contentType(MediaType.APPLICATION_JSON)
              ).andReturn();
                Course course = mapper.readValue(resultCourse.getResponse().getContentAsString(), Course.class);
                assertThat(course.getCourseName()).isEqualTo(request.getCourseName());

                Professor professor = getProfessor();
                MvcResult resultProfessor = mockMvc.perform(post("/api/v1/professors")
                        .content(mapper.writeValueAsString(professor))
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                ).andReturn();

                Professor responseProfessor = mapper.readValue(resultProfessor.getResponse().getContentAsString(), Professor.class);

                assertThat(responseProfessor.getName()).isEqualTo("Andrew NG");

                Student student = getStudent();
                MvcResult resultStudent = mockMvc.perform(post("/api/v1/students")
                        .content(mapper.writeValueAsString(student))
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                ).andReturn();

                Student responseStudent = mapper.readValue(resultStudent.getResponse().getContentAsString(), Student.class);

                assertThat(responseStudent.getName()).isEqualTo("saran");
            mockMvc.perform(post("/api/v1/courses/"+course.getId().toString()+"/professors")
                    .content(mapper.writeValueAsString(getProfessorDetails(responseProfessor)))
                    .accept(MediaType.APPLICATION_JSON)
                    .contentType(MediaType.APPLICATION_JSON)
            ).andExpect(status().isCreated());

            mockMvc.perform(post("/api/v1/courses/"+course.getId().toString()+"/students")
                    .content(mapper.writeValueAsString(getStudentDetails(responseStudent)))
                    .accept(MediaType.APPLICATION_JSON)
                    .contentType(MediaType.APPLICATION_JSON)
            ).andExpect(status().isCreated());


            MvcResult finalResult = mockMvc.perform(get("/api/v1/courses/"+course.getId())
                    .content(mapper.writeValueAsString(request))
                    .accept(MediaType.APPLICATION_JSON)
                    .contentType(MediaType.APPLICATION_JSON)
            ).andReturn();
            Course finalCourse = mapper.readValue(finalResult.getResponse().getContentAsString(), Course.class);
            assertThat(finalCourse.getEnrolledStudents()).size().isEqualTo(1);
            assertThat(finalCourse.getProfessors()).size().isEqualTo(1);

            Student student1 = getStudent();
            String student1Text = mockMvc.perform(post("/api/v1/students")
                    .content(mapper.writeValueAsString(student1))
                    .accept(MediaType.APPLICATION_JSON)
                    .contentType(MediaType.APPLICATION_JSON)
            ).andReturn().getResponse().getContentAsString();

            mockMvc.perform(post("/api/v1/courses/"+course.getId().toString()+"/students")
                    .content(student1Text)
                    .accept(MediaType.APPLICATION_JSON)
                    .contentType(MediaType.APPLICATION_JSON)
            ).andExpect(status().is(424));






        }


        private void createCourse(){

        }



        private CreateCourseRequest getCourseRequest(){
                return CreateCourseRequest.builder()
                        .courseName("ML")
                        .startDate(new Date())
                        .endDate(new Date())
                        .maximumCapacity(1)
                        .build();
        }

        private Professor getProfessor(){
                return Professor.builder()
                        .name("Andrew NG")
                        .build();
        }

        private Student getStudent(){
                return Student.builder().name("saran")
                        .build();
        }

        private ProfessorUdt getProfessorDetails(Professor professor){
            return ProfessorUdt.builder()
                    .id(professor.getId())
                    .name(professor.getName())
                    .build();
        }

        private StudentUdt getStudentDetails(Student student) {
            return StudentUdt.builder()
                    .id(student.getId())
                    .name(student.getName())
                    .build();
        }
}
