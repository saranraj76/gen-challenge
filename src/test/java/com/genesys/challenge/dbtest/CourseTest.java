package com.genesys.challenge.dbtest;

import com.genesys.challenge.models.Course;
import com.genesys.challenge.models.Professor;
import com.genesys.challenge.models.ProfessorUdt;
import com.genesys.challenge.models.StudentUdt;
import com.genesys.challenge.repository.CourseRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Date;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

@SpringBootTest
public class CourseTest {

    @Autowired
    CourseRepository courseRepo;

    //@Test
    public void createCourse() {
        Date today = new Date();
        today.setDate(0);
        Course course = Course.builder().id(UUID.randomUUID())
                .courseName("Machine Learning")
                .startDate(today)
                .endDate(today)
                .maximumCapacity(1)
                .enrolledStudents(new HashSet<>())
                .professors(new HashSet<>())
                .build();
        courseRepo.save(course);
    }

    //@Test
    public void addProfessor() {
        Optional<Course> course = courseRepo.findById(UUID.fromString("65a95d89-6978-44d7-86df-ecb26eced46e"));
        if (course.isPresent()) {
            Set<ProfessorUdt> professors;
            if (null == course.get().getProfessors()) {
                professors = new HashSet<>();
            } else {
                professors = course.get().getProfessors();
            }
            professors.add(ProfessorUdt.builder()
                    .name("Andrew NG")
                    .id(UUID.fromString("fffeb0d8-a487-4d93-b86b-4768fc5509f6")).build()
            );
            course.get().setProfessors(professors);
            courseRepo.save(course.get());
        }
    }

    @Test
    public void addStudent() {
        Optional<Course> course = courseRepo.findById(UUID.fromString("65a95d89-6978-44d7-86df-ecb26eced46e"));
        if (course.isPresent()) {
            Set<StudentUdt> students;
            if (null == course.get().getEnrolledStudents()) {
                students = new HashSet<>();
            } else {
                students = course.get().getEnrolledStudents();
            }
            if (course.get().getMaximumCapacity() >= students.size()) {
                students.add(StudentUdt.builder()
                        .name("saran")
                        .id(UUID.fromString("df7df96b-2dc6-42b1-93d2-8b82cfc0e9ff")).build()
                );
                course.get().setEnrolledStudents(students);
                courseRepo.save(course.get());
            }
        }
    }
}
