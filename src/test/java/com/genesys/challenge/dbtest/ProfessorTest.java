package com.genesys.challenge.dbtest;

import com.genesys.challenge.models.Professor;
import com.genesys.challenge.repository.ProfessorRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.UUID;

@SpringBootTest
public class ProfessorTest {

    @Autowired
    ProfessorRepository professorRepo;

    @Test
    public void createProfessor(){
        professorRepo.save(Professor.builder()
        .id(UUID.randomUUID())
                .name("Andrew NG")
                .build()
        );
    }
}
