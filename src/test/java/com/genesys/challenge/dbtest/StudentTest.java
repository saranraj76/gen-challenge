package com.genesys.challenge.dbtest;

import com.genesys.challenge.models.Student;
import com.genesys.challenge.repository.StudentRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.UUID;

@SpringBootTest
public class StudentTest {

    @Autowired
    StudentRepository studentRepo;

    @Test
    public void testCreateStudent(){
        Student student = Student.builder()
                .id(UUID.randomUUID())
                .name("Saran")
                .build();
        studentRepo.save(student);
    }

}
