# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

Gen challenge

### How do I get set up? ###

Docker based cassandra or a local cassandra to run the test (dint use embedded cassandra)


##normal package


mvn clean install // need local cassandra or else we need to skip the tests by adding -DskipTests

java -jar gen-challenge-0.0.1-SNAPSHOT.jar (This can be configured in a bash script & configured in a vm at .bashrc file start up)



##docker ##

mvn clean install // need local cassandra or else we need to skip the tests by adding -DskipTests

docker build -t gen-challenge:1.0 .

docker run -p 8080:8080 gen-challenge:1.0 -d


### Contribution guidelines ###

* Writing tests (only happy and custom exception tested defaults are kept for refactoring)
* Other guidelines (can be improved for scalable solution & what queries we are trying to achieve)


### scalability notes: ### 

The microservice is stateless and can scale to any number of instances (however the race conditions are not handled for the add students, since the capacity is subjective requirement)


The horizontal scaling supported rDBMS like mariadb or cockroach might have made the design much simpler. 
But to scale to 200 requests per second & the same support for a longer period of time no sql seems best fitting.

Mongo seems best fit for the model though it might not scale as good as cassandra.

Also the requirement seems to be more of write than read so opted to go with cassandra.


The best practices of cassandra was not fully opted like writting query in seperate tables like students_by_courses (since the query is subjective requirement).



### Thinks did not do: ### 

1) Logging though used lombok and easy to implement.
2) minimal exceptional Handling (just a poc code but covered the basics of how to extend the new exceptions)
3) Integration testing can be refactored to look better (coveres only 1 happy & 1 negative scenario)

